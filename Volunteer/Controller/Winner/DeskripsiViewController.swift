//
//  DeskripsiViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 02/08/21.
//

import UIKit

class DeskripsiViewController: UIViewController {

    @IBOutlet weak var tentangDeskripsiView: UIView!
    @IBOutlet weak var fokusDeskripsiView: UIView!
    @IBOutlet weak var tentangDeskripsiText: UILabel!
    @IBOutlet weak var fokusDeskripsiText: UILabel!
    var arrayPenyelenggara = penyelenggaraList
    var indeks : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        indeks = defaults.object(forKey: "Indeks NGO") as? Int
        if indeks != nil{
        }else {
            indeks = 0
        }

        tentangDeskripsiView.layer.cornerRadius = 8
        fokusDeskripsiView.layer.cornerRadius = 8
        
        tentangDeskripsiText.text = arrayPenyelenggara[indeks].deskripsi
        fokusDeskripsiText.text = arrayPenyelenggara[indeks].fokus.joined(separator: "\n")
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
