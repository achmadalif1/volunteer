//
//  DaftarViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 25/07/21.
//

import UIKit

class DaftarViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var namaTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var ulangPasswordTextfield: UITextField!
    @IBOutlet weak var gabungSekarangButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        view.addGestureRecognizer(tap)

        gabungSekarangButton.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    @IBAction func gabungSekarangButtonTapped(_ sender: Any) {
        if (emailTextfield.text == "" || namaTextfield.text == "" || passwordTextfield.text == "" || ulangPasswordTextfield.text == "") {
            let alert = UIAlertController(title: "Mohon isi semua sebelum melanjutkan", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("Textfield ada kosong")

                case .cancel:
                    print("cancel")

                case .destructive:
                    print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
        else if (passwordTextfield.text != ulangPasswordTextfield.text){
            let alert = UIAlertController(title: "Password tidak cocok", message: "Mohon periksa kembali password yang anda masukkan", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("Password tidak sama")

                case .cancel:
                    print("cancel")

                case .destructive:
                    print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
        else if (emailTextfield.text == email){
            let alert = UIAlertController(title: "Email sudah terdaftar", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
                print("Email sudah ada")
                }))
            self.present(alert, animated: true, completion: nil)
        }else{
            
            defaults.set(namaTextfield.text, forKey: "Nama")
            defaults.set(emailTextfield.text, forKey: "Email")
            defaults.set(passwordTextfield.text, forKey: "Password")
            
            
            let alert = UIAlertController(title: "Pendaftaran Berhasil", message: "Akun anda telah dibuat", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
                }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
