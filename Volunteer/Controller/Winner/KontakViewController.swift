//
//  KontakViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 02/08/21.
//

import UIKit

class KontakViewController: UIViewController {

    @IBOutlet weak var lokasiKontakView: UIView!
    @IBOutlet weak var websiteKontakView: UIView!
    @IBOutlet weak var emailKontakView: UIView!
    @IBOutlet weak var lokasiKontakText: UILabel!
    @IBOutlet weak var websiteKontakText: UILabel!
    @IBOutlet weak var emailKontakText: UILabel!
    var arrayPenyelenggara = penyelenggaraList
    var indeks : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        indeks = defaults.object(forKey: "Indeks NGO") as? Int
        if indeks != nil{
        }else {
            indeks = 0
        }

        lokasiKontakView.layer.cornerRadius = 8
        websiteKontakView.layer.cornerRadius = 8
        emailKontakView.layer.cornerRadius = 8
        
        lokasiKontakText.text = arrayPenyelenggara[indeks].lokasi
        websiteKontakText.text = arrayPenyelenggara[indeks].website
        emailKontakText.text = arrayPenyelenggara[indeks].email
    }
}
