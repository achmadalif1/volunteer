//
//  MasukViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 25/07/21.
//

import UIKit

class MasukViewController: UIViewController {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var daftarSekarangButton: UIButton!
    @IBOutlet weak var lupaPasswordButton: UIButton!
    @IBOutlet weak var masukButton: UIButton!
    @IBOutlet weak var masukAppleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        view.addGestureRecognizer(tap)
        logo.image = UIImage(named: "logoapp")
        masukButton.layer.cornerRadius = 10
        masukAppleButton.layer.cornerRadius = 10
        self.navigationController!.navigationBar.barTintColor = UIColor.orange

    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.barTintColor = UIColor.orange
    }
    
    @IBAction func masukButtonTapped(_ sender: Any) {
        let email = defaults.string(forKey: "Email")
        let password = defaults.string(forKey: "Password")
        if(emailTextfield.text == email && passwordTextfield.text == password){
            print("cocok")
        }else if(emailTextfield.text != email){
            let alert = UIAlertController(title: "Email tidak terdaftar", message: "Email yang anda masukkan tidak ada / tidak terdaftar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Coba Lagi", style: .destructive, handler: { action in
                                            alert.dismiss(animated: true, completion: nil)}))
            self.present(alert, animated: true, completion: nil)
        }else if(passwordTextfield.text != password){
            let alert = UIAlertController(title: "Password salah", message: "Password yang anda masukkan salah", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Coba Lagi", style: .destructive, handler: { action in
                                            alert.dismiss(animated: true, completion: nil)}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func masukAppleButtonTapped(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
