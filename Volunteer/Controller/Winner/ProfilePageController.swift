//
//  ProfilePageController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 16/07/21.
//

import UIKit


class ProfilePageController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var noticeView: UIView!
    @IBOutlet weak var fotoVolunteer: UIImageView!
    @IBOutlet weak var namaVolunteer: UILabel!
    @IBOutlet weak var emailVolunteer: UILabel!
    @IBOutlet weak var pekerjaanVolunteer: UILabel!
    @IBOutlet weak var minatVolunteer: UILabel!
    @IBOutlet weak var historyVolunteer: UITableView!
    @IBOutlet weak var minatBG: UIView!
    @IBOutlet weak var editProfileButton: UIBarButtonItem!
    
    @IBOutlet weak var regisButton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var toolbar: UINavigationItem!
    
    var arrayHistory = historyList
    var history = ["Membantu orang kurang mampu","Mengajar anak anak","membantu berjualan"]
    var issregis : Bool!
    var isregis : Bool!

    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        view.addGestureRecognizer(tap)
        let minat = defaults.object(forKey: "Minat") as? [String] ?? [String]()
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        if isregis {
            toolbar.rightBarButtonItem?.isEnabled = true
            noticeView.isHidden = true
            historyVolunteer.dataSource = self
            super.viewDidLoad()
            let defaults = UserDefaults.standard
            
            if(fotoVolunteer.image != nil){
            guard let data = UserDefaults.standard.data(forKey: "Foto") else { return }
            let decoded = try! PropertyListDecoder().decode(Data.self, from: data)
            fotoVolunteer.image = UIImage(data: decoded)
            }
            
            if(fotoVolunteer.image == nil){
                fotoVolunteer.image = UIImage(named: "Image")
            }
            
            if(nama == nil || nama == ""){namaVolunteer.text = "-"}
            else{namaVolunteer.text = defaults.string(forKey: "Nama")}
            if(email == nil || email == ""){emailVolunteer.text = "-"}
            else{emailVolunteer.text = defaults.string(forKey: "Email")}
            if(pekerjaan == nil || pekerjaan == ""){pekerjaanVolunteer.text = "-"}
            else{pekerjaanVolunteer.text = defaults.string(forKey: "Pekerjaan")}
            if(minat == []){minatVolunteer.text = "-"}
            else{minatVolunteer.text = "- "
                minatVolunteer.text! += minat.joined(separator: "\n- ")}
            
            
            
            fotoVolunteer.clipsToBounds = true
            fotoVolunteer.contentMode = .scaleAspectFill
            fotoVolunteer.layer.cornerRadius = fotoVolunteer.frame.size.width / 2
            fotoVolunteer.layer.borderWidth = 1
            minatBG.layer.cornerRadius = 8
            
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }else {
            noticeView.isHidden = false
            regisButton.layer.cornerRadius = 15
            regisButton.layer.borderColor = UIColor.black.cgColor
            regisButton.clipsToBounds = true
            label.text = "Mohon masuk untuk melihat halaman ini"
            //toolbar.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem = nil
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = UIColor.systemGray6
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            
        }
        
    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let nama = defaults.string(forKey: "Nama")
        let email = defaults.string(forKey: "Email")
        let pekerjaan = defaults.string(forKey: "Pekerjaan")
        let minat = defaults.object(forKey: "Minat") as? [String] ?? [String]()
        
        if(fotoVolunteer.image != nil){
        guard let data = UserDefaults.standard.data(forKey: "Foto") else { return }
        let decoded = try! PropertyListDecoder().decode(Data.self, from: data)
        fotoVolunteer.image = UIImage(data: decoded)
        }
        
        if(fotoVolunteer.image == nil){
            fotoVolunteer.image = UIImage(named: "Image")
        }
        
        if(nama == nil || nama == ""){namaVolunteer.text = "-"}
        else{namaVolunteer.text = nama}
        if(email == nil || email == ""){emailVolunteer.text = "-"}
        else{emailVolunteer.text = email}
        if(pekerjaan == nil || pekerjaan == ""){pekerjaanVolunteer.text = "-"}
        else{pekerjaanVolunteer.text = pekerjaan}
        if(minat == []){minatVolunteer.text = "-"}
        else{minatVolunteer.text = "- "
            minatVolunteer.text! += minat.joined(separator: "\n- ") }
        
        fotoVolunteer.clipsToBounds = true
        fotoVolunteer.contentMode = .scaleAspectFill
        fotoVolunteer.layer.cornerRadius = fotoVolunteer.frame.size.width / 2
        //fotoVolunteer.layer.borderWidth = 1
        minatBG.layer.cornerRadius = 8
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyVolunteer.dequeueReusableCell(withIdentifier: "HistoryProfilTableViewCell", for: indexPath) as! ProfilPageTableViewCell
        cell.tanggalHistory.text = arrayHistory[indexPath.row].tanggal
        cell.namaAktivitasHistory.text = arrayHistory[indexPath.row].nama
        return cell     
    }
    
    @IBAction func editButton(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(identifier: "UbahProfilViewController") as! EditProfileController
        controller.delegate = self
        let navController = UINavigationController(rootViewController: controller)
        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func masukButtonTapped(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoginRegister", bundle:nil)
        if let ngoprofile = storyBoard.instantiateViewController(identifier: "MasukViewController") as? MasukViewController{
            self.navigationController?.pushViewController(ngoprofile, animated: true)
        }
    }
    
}

extension ProfilePageController: EditProfileControllerDelegate{
    func didChange(namad: String, emaild: String, pekerjaand: String, minatd: String, fotod: UIImage) {
        if(namad == ""){namaVolunteer.text = "-"}
        else{namaVolunteer.text = namad}
        if(emaild == ""){emailVolunteer.text = "-"}
        else{emailVolunteer.text = emaild}
        if(pekerjaand == ""){pekerjaanVolunteer.text = "-"}
        else{pekerjaanVolunteer.text = pekerjaand}
        if(minatd == ""){minatVolunteer.text = "-"}
        else{minatVolunteer.text = minatd}
        fotoVolunteer.image = fotod
    }
}

class ProfilPageTableViewCell : UITableViewCell{
    
    @IBOutlet weak var tanggalHistory: UILabel!
    @IBOutlet weak var namaAktivitasHistory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
