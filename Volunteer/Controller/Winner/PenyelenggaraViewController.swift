//
//  PenyelenggaraViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 02/08/21.
//

import UIKit

class PenyelenggaraViewController: UIViewController,UINavigationControllerDelegate {

    @IBOutlet weak var fotoPenyelenggara: UIImageView!
    @IBOutlet weak var namaPenyelenggara: UILabel!
    @IBOutlet weak var kotaPenyelenggara: UILabel!
    @IBOutlet weak var deskripsiView : UIView!
    @IBOutlet weak var programView : UIView!
    @IBOutlet weak var kontakView : UIView!
    var indeks : Int!
    var arrayPenyelenggara = penyelenggaraList
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indeks = defaults.object(forKey: "Indeks NGO") as? Int
        if indeks != nil{
        }else {
            indeks = 0
        }

        namaPenyelenggara.text = arrayPenyelenggara[indeks].nama
        kotaPenyelenggara.text = arrayPenyelenggara[indeks].kota
        fotoPenyelenggara.image = UIImage(named: arrayPenyelenggara[indeks].foto)
        
        fotoPenyelenggara.clipsToBounds = true
        fotoPenyelenggara.contentMode = .scaleAspectFill
        fotoPenyelenggara.layer.cornerRadius = fotoPenyelenggara.frame.size.width / 2
        fotoPenyelenggara.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        indeks = defaults.object(forKey: "Indeks NGO") as? Int
        if indeks != nil{
        }else {
            indeks = 0
        }
        
        namaPenyelenggara.text = arrayPenyelenggara[indeks].nama
        kotaPenyelenggara.text = arrayPenyelenggara[indeks].kota
        fotoPenyelenggara.image = UIImage(named: arrayPenyelenggara[indeks].foto)
        
    }
    
    @IBAction func penyelenggaraSegmented(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            
            deskripsiView.alpha = 1
            programView.alpha = 0
            kontakView.alpha = 0
        }
        else if sender.selectedSegmentIndex == 1{
            
            deskripsiView.alpha = 0
            programView.alpha = 1
            kontakView.alpha = 0
        }else if sender.selectedSegmentIndex == 2{
            
            deskripsiView.alpha = 0
            programView.alpha = 0
            kontakView.alpha = 1
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
