//
//  ProgramViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 02/08/21.
//

import UIKit

class ProgramViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return program.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = daftarAktivitasPenyelenggara.dequeueReusableCell(withIdentifier: "DaftarProgramPenyelenggaraViewCell", for: indexPath) as! ProgramViewCell
        cell.fotoAktivitasProgram.image = UIImage(named: program[indexPath.row].photoAssetName)
        cell.namaAktivitasProgram.text = program[indexPath.row].nama
        cell.tanggalAktivitasProgram.text = program[indexPath.row].tanggal
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectItemAt indexPath: IndexPath ) {
        if let detailProgramViewController = self.storyboard?.instantiateViewController(identifier: DetailProgramViewController.indentifier) as? DetailProgramViewController{
            detailProgramViewController.program = program[indexPath.row]
            self.navigationController?.pushViewController(detailProgramViewController, animated: true)
        }
    }
    
    @IBOutlet weak var daftarAktivitasPenyelenggara: UITableView!
    var arrayprogram = ProgramManager().program
    var indeks : Int!
    var program : [Program]! = []
    
    override func viewDidLoad() {
        daftarAktivitasPenyelenggara.dataSource = self
        super.viewDidLoad()
        indeks = defaults.object(forKey: "Indeks NGO") as? Int
        if indeks != nil{
        }else {
            indeks = 0
        }
        for data in arrayprogram{
            if data.penyelenggara.lowercased().contains(penyelenggaraList[indeks].nama.lowercased()){
                program.append(data)
            }
        }
        print(arrayprogram.count)
        print(arrayprogram[0].nama)

    }

}

class ProgramViewCell : UITableViewCell{
    

    @IBOutlet weak var fotoAktivitasProgram: UIImageView!
    @IBOutlet weak var namaAktivitasProgram: UILabel!
    @IBOutlet weak var tanggalAktivitasProgram: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fotoAktivitasProgram.layer.cornerRadius = 15
        fotoAktivitasProgram.clipsToBounds = true
        fotoAktivitasProgram.layer.borderWidth = 1
        fotoAktivitasProgram.contentMode = .scaleAspectFill
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

