//
//  EditProfileController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 19/07/21.
//

import UIKit


var namaVolunteer : String!
var emailVolunteer : String!
var pekerjaanVolunteer : String!

protocol EditProfileControllerDelegate {
    func didChange(namad:String, emaild: String, pekerjaand:String, minatd:String, fotod:UIImage)
}

class EditProfileController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var delegate : EditProfileControllerDelegate!
    @IBOutlet weak var fotoVolunteer: UIImageView!
    @IBOutlet weak var editFotoVolunteer: UIButton!
    @IBOutlet weak var namaVolunteerTF: UITextField!
    @IBOutlet weak var emailVolunteerTF: UITextField!
    @IBOutlet weak var pekerjaanVolunteerTF: UITextField!
    @IBOutlet weak var jumlahMinatVolunteer: UILabel!
    @IBOutlet weak var batalNavBar: UIBarButtonItem!
    @IBOutlet weak var selesaiNavBar: UIBarButtonItem!
    @IBOutlet weak var minatLabel: UILabel!
    @IBOutlet weak var bgMinat: UIButton!
    var imagepicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let minat = defaults.object(forKey: "Minat") as? [String] ?? [String]()
        let defaults = UserDefaults.standard
        
        if(fotoVolunteer.image != nil){
        guard let data = UserDefaults.standard.data(forKey: "Foto") else { return }
        let decoded = try! PropertyListDecoder().decode(Data.self, from: data)
        fotoVolunteer.image = UIImage(data: decoded)
        }
            
        if(fotoVolunteer.image == nil){
            fotoVolunteer.image = UIImage(named: "Image")
        }
        
                //fotoVolunteer.image = UIImage(named: "\(arrayVolunteer[0].foto)")
        fotoVolunteer.clipsToBounds = true
        fotoVolunteer.contentMode = .scaleAspectFill
        fotoVolunteer.layer.cornerRadius = fotoVolunteer.frame.size.width / 2
        fotoVolunteer.layer.borderWidth = 1
        
        editFotoVolunteer.layer.cornerRadius = editFotoVolunteer.frame.size.width/2
        bgMinat.layer.cornerRadius = 8
        
        namaVolunteerTF.text = defaults.string(forKey: "Nama")
        emailVolunteerTF.text = defaults.string(forKey: "Email")
        pekerjaanVolunteerTF.text = defaults.string(forKey: "Pekerjaan")
        jumlahMinatVolunteer.text = "\(jumlahMinat)"
        minatLabel.text = "- "
        minatLabel.text! += minat.joined(separator: "\n- ")
    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        let minat = defaults.object(forKey: "Minat") as? [String] ?? [String]()
        
        if(fotoVolunteer.image != nil){
        guard let data = UserDefaults.standard.data(forKey: "Foto") else { return }
        let decoded = try! PropertyListDecoder().decode(Data.self, from: data)
        fotoVolunteer.image = UIImage(data: decoded)
        }
        
        if(fotoVolunteer.image == nil){
            fotoVolunteer.image = UIImage(named: "Image")
        }
        namaVolunteerTF.text = defaults.string(forKey: "Nama")
        emailVolunteerTF.text = defaults.string(forKey: "Email")
        pekerjaanVolunteerTF.text = defaults.string(forKey: "Pekerjaan")
        jumlahMinatVolunteer.text = "\(jumlahMinat)"
        
        fotoVolunteer.clipsToBounds = true
        fotoVolunteer.contentMode = .scaleAspectFill
        fotoVolunteer.layer.cornerRadius = fotoVolunteer.frame.size.width / 2
        //fotoVolunteer.layer.borderWidth = 1
        editFotoVolunteer.layer.cornerRadius = editFotoVolunteer.frame.size.width/2
        bgMinat.layer.cornerRadius = 8
        
        namaVolunteerTF.text = defaults.string(forKey: "Nama")
        emailVolunteerTF.text = defaults.string(forKey: "Email")
        pekerjaanVolunteerTF.text = defaults.string(forKey: "Pekerjaan")
        jumlahMinatVolunteer.text = "\(jumlahMinat)"
        minatLabel.text = "- "
        minatLabel.text! += minat.joined(separator: "\n- ")
    }
    
    @IBAction func EditFotoButton(_ sender: Any) {
        let alert = UIAlertController(title: "Ubah Foto Profil", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Pilih Foto", style: .default, handler: { action in
            switch action.style{
            case .default:
                self.imagepicker.sourceType = .photoLibrary
                self.imagepicker.allowsEditing = true
                self.imagepicker.delegate = self
                self.present(self.imagepicker, animated: true)
                print("default")

            case .cancel:
                print("cancel")

            case .destructive:
                print("destructive")
            }}))
        alert.addAction(UIAlertAction(title: "Ambil Foto", style: .default, handler: { action in
            switch action.style{
            case .default:
                self.imagepicker.sourceType = .camera
                self.imagepicker.allowsEditing = true
                self.imagepicker.delegate = self
                self.present(self.imagepicker, animated: true)

                print("default")

            case .cancel:
                print("cancel")

            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else { return }
        self.fotoVolunteer.image = image
    }
    
    @IBAction func MinatButton(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(identifier: "MinatTableViewController") as! MinatTableViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func BatalButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SelesaiButton(_ sender: Any) {
        if(fotoVolunteer.image ==  nil){
            fotoVolunteer.image = UIImage(named: "Image")
        }
        defaults.set(namaVolunteerTF.text!, forKey: "Nama")
        defaults.set(emailVolunteerTF.text!, forKey: "Email")
        defaults.set(pekerjaanVolunteerTF.text!, forKey: "Pekerjaan")
        let data = fotoVolunteer.image?.jpegData(compressionQuality: 0.5)
        let encoded = try! PropertyListEncoder().encode(data)
        defaults.set(encoded, forKey: "Foto")
        delegate?.didChange(namad: namaVolunteerTF.text ?? "", emaild: emailVolunteerTF.text ?? "", pekerjaand: pekerjaanVolunteerTF.text ?? "", minatd: minatLabel.text ?? "", fotod: fotoVolunteer.image!)
        dismiss(animated: true)
    }

}

extension EditProfileController: MinatTableViewControllerDelegate{
    func didChange(minatd: [String]) {
        minatLabel.text = "- "
        minatLabel.text! += minatd.joined(separator: "\n- ")
    }
    
}
