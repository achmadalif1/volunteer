//
//  LupaPasswordViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 25/07/21.
//

import UIKit

class LupaPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var kirimKodeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    
    @IBAction func kirimKodeButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Kode Verifikasi Telah Dikirim", message: "Silakan cek email anda untuk melakukan verifikasi kepemilikan akun anda.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")

            case .cancel:
                print("cancel")

            case .destructive:
                print("destructive")
            }}))
        
    self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
