//
//  MinatTableViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 19/07/21.
//

import UIKit

protocol MinatTableViewControllerDelegate {
    func didChange(minatd:[String])
}

class MinatTableViewController: UITableViewController {
    var delegate : MinatTableViewControllerDelegate!
    @IBOutlet var pilihanMinat: UITableView!
    var minatArray: [String] = []
    let arrayminat = [
        "Dunia Tanpa Kemiskinan",
        "Dunia Tanpa Kelaparan",
        "Kesehatan Fisik",
        "Kesehatan Mental",
        "Pendidikan Berkualitas",
        "Kesetaraan Gender",
        "Pemberdayaan Perempuan",
        "Akses Air Bersih dan Sanitasi",
        "Energi Bersih dan Terjangkau",
        "Pekerjaan yang Layak",
        "Pertumbuhan Ekonomi",
        "Kesejahteraan Merata",
        "Penanganan Perubahan Iklim",
        "Menjaga Ekosistem Laut",
        "Menjaga Ekosistem Hutan",
        "Menjaga Keberlangsungan Hidup Tumbuhan dan Hewan",
        "Menjaga Perdamaian Dunia",
        "Kerjasama Internasional",
    ]
    
    override func viewDidLoad() {
        jumlahMinat = 0
        pilihanMinat.dataSource = self
        super.viewDidLoad()
        self.navigationItem.title = "Minat Saya"
        let customView =  UIView(frame: CGRect(x: 0,y: 0,width: self.tableView.frame.width,height: 40))
        customView.backgroundColor = .clear
        let titleLabel = UILabel(frame: CGRect(x:5,y: 5 ,width:customView.frame.width,height:40))
        titleLabel.text  = "    Anda dapat memilih maksimal 3 minat"
        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        customView.addSubview(titleLabel)
        tableView.tableFooterView = customView
        minat = defaults.object(forKey: "Minat") as? [String] ?? []
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayminat.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DaftarMinatTableViewCell", for: indexPath)
        cell.textLabel?.text = arrayminat[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        for object in minat {
            if object == cell.textLabel?.text {
                cell.accessoryType = .checkmark
                jumlahMinat+=1
                minatArray.append(cell.textLabel?.text ?? "")
            }
        }
        defaults.set(minatArray,forKey: "Minat")
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath as IndexPath) {
            
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                jumlahMinat-=1
                for object in minatArray {
                    if object == cell.textLabel?.text {
                        minatArray.remove(at: minatArray.firstIndex(of: cell.textLabel?.text ?? "")!)
                    }
                }
                defaults.set(jumlahMinat, forKey: "JumlahMinat")
            }else if cell.accessoryType == .none{
                if (jumlahMinat >= 3){
                    print("Minat Maksimal 3")
                    cell.accessoryType = .none
                }else{
                    jumlahMinat+=1
                    cell.accessoryType = .checkmark
                    minatArray.append(cell.textLabel?.text ?? "")
                }
                print(jumlahMinat)
                defaults.set(jumlahMinat, forKey: "JumlahMinat")
            }
            print(minatArray)
            defaults.set(minatArray,forKey: "Minat")
            defaults.synchronize()
        }
        delegate.didChange(minatd: minatArray)
    }
}
