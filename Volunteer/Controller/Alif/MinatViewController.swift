//
//  MinatTableViewController.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 19/07/21.
//

import UIKit

class MinatViewController: UITableViewController {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet var pilihanMinat: UITableView!
    var arrayminatvolunteer : [String] = []
    var arrayminat = [
        "Dunia Tanpa Kemiskinan",
        "Dunia Tanpa Kelaparan",
        "Kesehatan Fisik",
        "Kesehatan Mental",
        "Pendidikan Berkualitas",
        "Kesetaraan Gender",
        "Pemberdayaan Perempuan",
        "Akses Air Bersih dan Sanitasi",
        "Energi Bersih dan Terjangkau",
        "Pekerjaan yang Layak",
        "Pertumbuhan Ekonomi",
        "Kesejahteraan Merata",
        "Penanganan Perubahan Iklim",
        "Menjaga Ekosistem Laut",
        "Menjaga Ekosistem Hutan",
        "Menjaga Keberlangsungan Hidup Tumbuhan dan Hewan",
        "Menjaga Perdamaian Dunia",
        "Kerjasama Internasional",
    ]
    var jumlahminat :Int = 0
    
    override func viewDidLoad() {
        defaults.set(true,forKey: "Registrasi")
        pilihanMinat.dataSource = self
        super.viewDidLoad()
        hidedoneButton()
        let header = UIView(frame: CGRect(x: 0, y: 0, width: 380 , height: 100))
        header.layer.backgroundColor = UIColor(red: 196, green: 196, blue: 500, alpha:1).cgColor
        let label = UILabel(frame: header.bounds)
        label.text = "Tambahkan bidang volunteer yang anda minati agar kita dapat mempersonalisasikan aplikasi ini untukmu. Minatmu bersifat rahasia. (Anda dapat memilih maximal 3 minat)"
        label.textAlignment = .center
        label.numberOfLines = 0
        header.addSubview(label)
        tableView.tableHeaderView = header
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayminat.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DaftarMinatTableViewCell", for: indexPath)
        cell.textLabel?.text = arrayminat[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                jumlahminat-=1
                for object in arrayminatvolunteer {
                    if object == cell.textLabel?.text {
                        arrayminatvolunteer.remove(at: arrayminatvolunteer.firstIndex(of: cell.textLabel?.text ?? "")!)
                    }
                }
                defaults.set(arrayminatvolunteer,forKey: "Minat")
                hidedoneButton()
                
            }else if cell.accessoryType == .none{
                if (jumlahminat == 3){
                    print("Minat Maksimal 3")
                    cell.accessoryType = .none
                }else{
                    jumlahminat+=1
                    cell.accessoryType = .checkmark
                    arrayminatvolunteer.append(cell.textLabel?.text ?? "")
                    defaults.set(arrayminatvolunteer,forKey: "Minat")
                    print(arrayminatvolunteer)
                    print(jumlahminat)
                    hidedoneButton()
                }
            }
        }
    }
    func hidedoneButton() {
        if arrayminatvolunteer.count == 0{
            doneButton.isEnabled = false
        }else{
            doneButton.isEnabled = true
            defaults.set(arrayminatvolunteer, forKey: "Minat")
            defaults.set(jumlahminat, forKey: "JumlahMinat")
        }
    }
    
}
