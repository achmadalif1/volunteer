//
//  SeeAllView.swift
//  Home Appstore
//
//  Created by Ahmad Nur Alifullah on 25/07/21.
//

import UIKit

class SeeAllView: UIViewController {
    static let indentifier = "SeeAllView"
    @IBOutlet weak var collectionView: UICollectionView!
    var indeks:Int!
    var sections :[Section] = SectionManager().section
    var program : [Program]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sections = SectionManager().section
        program = sections[indeks].items
        collectionView.frame = view.bounds
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 349 , height: 100)
        collectionView.collectionViewLayout = layout
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        collectionView.register(MediumTableCell.nib(), forCellWithReuseIdentifier: MediumTableCell.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    override func viewDidAppear(_ animated: Bool) {
        sections = SectionManager().section
        program = sections[indeks].items
        collectionView.reloadData()
    }
}
extension SeeAllView: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return program.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
            fatalError("Unable to dequeue \(MediumTableCell.self)")
        }
        
        cell.configure(with: program[indexPath.row])
        return cell
        
    }
}
extension SeeAllView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath ) {
       
        collectionView.deselectItem(at: indexPath, animated: true)
        if let detailProgramViewController = self.storyboard?.instantiateViewController(identifier: DetailProgramViewController.indentifier) as? DetailProgramViewController{
            detailProgramViewController.program = program[indexPath.row]
            self.navigationController?.pushViewController(detailProgramViewController, animated: true)
        }
    }
}
       
            

