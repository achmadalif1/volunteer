//
//  ViewController.swift
//  Home Appstore
//
//  Created by Ahmad Nur Alifullah on 24/07/21.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var program:[Program] = ProgramManager().program
    var sections :[Section] = SectionManager().section
    var penyelenggara :[Penyelenggara] = penyelenggaraList
    var isregis : Bool!
    var issregis : Bool!
    @IBOutlet weak var searchCollection: UICollectionView!
    var filteredData : [Program]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        defaults.set([],forKey: "id")
        filteredData = []
        searchCollection.isHidden = true
        setupSearchCollection()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (ViewController.DismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        program = ProgramManager().program
        sections = SectionManager().section
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        collectionView.frame = view.bounds
        if isregis{
            collectionView.collectionViewLayout = createCompositionalLayout()
        }else{
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: 349 , height: 100)
            collectionView.collectionViewLayout = layout
        }
    
        
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        collectionView.register(SectionHeader.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeader.reuseIdentifier)
        collectionView.register(FeaturedCell.nib(), forCellWithReuseIdentifier: FeaturedCell.reuseIdentifier)
        collectionView.register(MediumTableCell.nib(), forCellWithReuseIdentifier: MediumTableCell.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        collectionView.register(SmallTableCell.self, forCellWithReuseIdentifier: SmallTableCell.reuseIdentifier)
        // Do any additional setup after loading the view.
        setUpNavBar()
        searchBar.delegate = self
    }
    @objc func DismissKeyboard(){
    view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        filteredData = []
        searchCollection.isHidden = true
        setupSearchCollection()
        
        program = ProgramManager().program
        sections = SectionManager().section
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        collectionView.frame = view.bounds
        if isregis{
            collectionView.collectionViewLayout = createCompositionalLayout()
        }else{
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: 349 , height: 100)
            collectionView.collectionViewLayout = layout
        }
        
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        collectionView.register(SectionHeader.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeader.reuseIdentifier)
        collectionView.register(FeaturedCell.nib(), forCellWithReuseIdentifier: FeaturedCell.reuseIdentifier)
        collectionView.register(MediumTableCell.nib(), forCellWithReuseIdentifier: MediumTableCell.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        collectionView.register(SmallTableCell.self, forCellWithReuseIdentifier: SmallTableCell.reuseIdentifier)
        // Do any additional setup after loading the view.
        setUpNavBar()
        searchBar.delegate = self
        
        
    }
    func setupSearchCollection(){
        searchCollection.frame = view.bounds
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 349 , height: 100)
        searchCollection.collectionViewLayout = layout
        searchCollection.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        searchCollection.backgroundColor = .systemBackground
        searchCollection.register(MediumTableCell.nib(), forCellWithReuseIdentifier: MediumTableCell.reuseIdentifier)
        searchCollection.delegate = self
        searchCollection.dataSource = self
    }
    
    func setUpNavBar() {
           searchBar.sizeToFit()
           searchBar.searchBarStyle = .minimal
           searchBar.placeholder = "Nama, Penyelenggara, Interest, Lokasi"
           searchBar.tintColor = UIColor.lightGray
           searchBar.barTintColor = UIColor.lightGray
           searchBar.isTranslucent = true
    }
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        collectionView.isHidden = true
//        searchCollection.isHidden = false
//    }
    //merged
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        collectionView.isHidden = false
        searchCollection.isHidden = true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != nil {
            collectionView.isHidden = true
            searchCollection.isHidden = false
            filteredData = []
            var found : Bool = false
            for data in program {
                found = false
                if data.nama.lowercased().contains(searchText.lowercased()){
                    found = true
                }else if data.penyelenggara.lowercased().contains(searchText.lowercased()){
                    found = true
                }else if data.lokasi.lowercased().contains(searchText.lowercased()){
                    found = true
                }
                for i in data.kategori{
                    if i.lowercased().contains(searchText.lowercased()){
                        found = true
                    }
                }
                if found == true{
                    filteredData.append(data)
                }
            }
            self.searchCollection.reloadData()
        }else{
            collectionView.isHidden = false
            searchCollection.isHidden = true
        }
        
    }
    
    @objc func buttonAction0(sender: Any) {
        if let seeallview = self.storyboard?.instantiateViewController(identifier: SeeAllView.indentifier) as? SeeAllView{
            defaults.set(1,forKey: "indeks")
            seeallview.indeks = 1
            self.navigationController?.pushViewController(seeallview, animated: true)
        }
    }
    
    @objc func buttonAction1(sender: Any) {
        if let seeallview = self.storyboard?.instantiateViewController(identifier: SeeAllView.indentifier) as? SeeAllView{
            defaults.set(2,forKey: "indeks")
            seeallview.indeks = 2
            self.navigationController?.pushViewController(seeallview, animated: true)
        }
    }
    
    func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            let section = self.sections[sectionIndex]

            switch section.type {
            case "mediumTable":
                return self.createMediumTableSection(using: section)
            case "smallTable":
                return self.createSmallTableSection(using: section)
            default:
                return self.createFeaturedSection(using: section)
            }
        }
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 20
        layout.configuration = config
        return layout
    }
    
    func createFeaturedSection(using section: Section) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))

        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)

        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93), heightDimension: .estimated(300))
        let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        layoutSection.orthogonalScrollingBehavior = .groupPagingCentered
        return layoutSection
    }

    func createMediumTableSection(using section: Section) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.25))

        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)

        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93), heightDimension: .fractionalWidth(1))
        let layoutGroup = NSCollectionLayoutGroup.vertical(layoutSize: layoutGroupSize, subitems: [layoutItem])

        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        layoutSection.orthogonalScrollingBehavior = .groupPagingCentered

        let layoutSectionHeader = createSectionHeader()
        layoutSection.boundarySupplementaryItems = [layoutSectionHeader]

        return layoutSection
    }

    func createSmallTableSection(using section: Section) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.2))
        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0)

        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93), heightDimension: .estimated(200))
        let layoutGroup = NSCollectionLayoutGroup.vertical(layoutSize: layoutGroupSize, subitems: [layoutItem])

        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        let layoutSectionHeader = createSectionHeader()
        layoutSection.boundarySupplementaryItems = [layoutSectionHeader]

        return layoutSection
    }

    func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
        let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93), heightDimension: .estimated(80))
        let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        return layoutSectionHeader
    }
    
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView == self.collectionView{
            var section = sections[indexPath.section]
            let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionHeader.reuseIdentifier, for: indexPath) as? SectionHeader
            sectionHeader?.title.text = section.title
            sectionHeader?.subtitle.text = section.subtitle
            if indexPath.section == 1{
                sectionHeader?.seeallButton.addTarget(self, action: #selector(buttonAction0), for: .touchUpInside)
            }else {
                sectionHeader?.seeallButton.addTarget(self, action: #selector(buttonAction1), for: .touchUpInside)
            }
            return sectionHeader ?? UICollectionReusableView()
        }else {
            return UICollectionReusableView()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.collectionView{
            if isregis {
                return sections.count
            }else {
                return 1
            }
        }else {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView{
            if isregis{
                if section == 0 {
                    return penyelenggara.count
                }
                return sections[section].items.count
            }else {
                return program.count
            }
        }else{
            if filteredData.count > 0 {
                return filteredData.count
            }else {
                return program.count
            }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView{
            if isregis{
                switch self.sections[indexPath.section].type {
                case "mediumTable":
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
                        fatalError("Unable to dequeue \(MediumTableCell.self)")
                    }
                    
                    cell.configure(with: sections[indexPath.section].items[indexPath.row])
                    return cell
                default:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCell.reuseIdentifier, for: indexPath) as? FeaturedCell else {
                        fatalError("Unable to dequeue \(FeaturedCell.self)")
                    }
                    cell.configure(with: penyelenggara[indexPath.row])
                    return cell
                    
                }
            }else{
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
                    fatalError("Unable to dequeue \(MediumTableCell.self)")
                }
                cell.configure(with: program[indexPath.row])
                return cell
            }
        }else{
            if filteredData.count > 0{
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
                    fatalError("Unable to dequeue \(MediumTableCell.self)")
                }
                cell.configure(with: filteredData[indexPath.row])
                return cell
            }else{
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
                    fatalError("Unable to dequeue \(MediumTableCell.self)")
                }
                cell.configure(with: program[indexPath.row])
                return cell
            }
            
        }
        
    }
}
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath ) {
        if collectionView == self.collectionView{
            if let detailProgramViewController = self.storyboard?.instantiateViewController(identifier: DetailProgramViewController.indentifier) as? DetailProgramViewController{
                if isregis{
                    if indexPath.section == 0 {
                        let storyBoard : UIStoryboard = UIStoryboard(name: "NgoProfilePage", bundle:nil)
                        if let ngoprofile = storyBoard.instantiateViewController(identifier: "PenyelenggaraViewController") as? PenyelenggaraViewController{
                            defaults.set(indexPath.row,forKey: "Indeks NGO")
                            self.navigationController?.pushViewController(ngoprofile, animated: true)
                        }
                    }else{
                        detailProgramViewController.program = sections[indexPath.section].items[indexPath.row]
                        self.navigationController?.pushViewController(detailProgramViewController, animated: true)
                    }
                    
                }else{
                    detailProgramViewController.program = program[indexPath.row]
                    self.navigationController?.pushViewController(detailProgramViewController, animated: true)
                }
            }
        }else{
            if let detailProgramViewController = self.storyboard?.instantiateViewController(identifier: DetailProgramViewController.indentifier) as? DetailProgramViewController{
                if filteredData.count > 0 {
                    detailProgramViewController.program = filteredData[indexPath.row]
                }else{
                    detailProgramViewController.program = program[indexPath.row]
                }
                
                self.navigationController?.pushViewController(detailProgramViewController, animated: true)
            }
        }
    }
}


       
            
