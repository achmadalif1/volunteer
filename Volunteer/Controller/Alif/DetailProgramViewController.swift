//
//  DetailProgramViewController.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 15/07/21.
//

import UIKit

class DetailProgramViewController: UIViewController {
    static let indentifier = "DetailProgramViewController"
    var program:Program!
    @IBOutlet weak var gb_aktiv: UIImageView!
    @IBOutlet weak var nama_detail: UILabel!
    @IBOutlet weak var lokasi_detail: UILabel!
    @IBOutlet weak var detail_collection: UICollectionView!
    @IBOutlet weak var daftar_detailout: UIButton!
    @IBOutlet weak var todolist: UIButton!
    var id : [Int]!
    var idadata : [Int]!
    var indexCellSelected:IndexPath?
    var issregis : Bool!
    var isregis : Bool!
    @IBOutlet weak var mapsbtn: UIButton!
    var  simpan_alvin3 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        
        
        idadata = defaults.object(forKey: "id") as? [Int]
        if idadata != nil{
            id = idadata
        }else {
            id = []
        }
        hideDaftar()
        
        gb_aktiv.image = UIImage(named: "\(program.photoAssetName)")
        nama_detail.text = program.nama
        lokasi_detail.text = program.lokasi
        daftar_detailout.layer.cornerRadius = 5
        daftar_detailout.layer.shadowRadius = 10
        daftar_detailout.layer.shadowColor = UIColor(red: 149, green: 165, blue: 166, alpha: 1.0).cgColor
        
        todolist.layer.cornerRadius = 5
        todolist.layer.shadowRadius = 10
        
        detail_collection.register(DetProgCollectionViewCell.nib(), forCellWithReuseIdentifier: DetProgCollectionViewCell.indentifier)
        detail_collection.delegate = self
        detail_collection.dataSource = self
        
        
    }
    
    @IBAction func mapsbtn_tapped(_ sender: Any) {
        self.simpan_alvin3 = program.coordinate_save
        print("test1")
        performSegue(withIdentifier: "maps3ViewController", sender: self)
        print("test2")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var vc = segue.destination as! maps3ViewController
            vc.simpan_alvin2 = self.simpan_alvin3
        }

    override func viewDidAppear(_ animated: Bool) {
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        idadata = defaults.object(forKey: "id") as? [Int]
        if idadata != nil{
            id = idadata
        }else {
            id = []
        }
        hideDaftar()
    }
    
    func hideDaftar() {
        if id.count != 0 {
            for i in id{
                if i == program.id{
                    daftar_detailout.isHidden = true
                    daftar_detailout.isEnabled = false
                }
            }
            
        }
    }
    
    @IBAction func daftar_detail(_ sender: Any) {
        if isregis {
            daftar_detailout.isHidden = true
            daftar_detailout.isEnabled = false
            id.append(program.id)
            defaults.set(id,forKey: "id")
    //        delegate.didChange(id: id)
            showAlert()
        }else{
            let alert = UIAlertController(title: "Peringatan", message: "Akun anda belum terdaftar, silahkan login di halaman profile", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {action in
                print("gagal daftar")
            }))
            present(alert, animated: true)
            
        }
    }
    
    func showAlert () {
        let alert = UIAlertController(title: "Pendaftaran Berhasil", message: "Detail dari project ini dapat diakses melalui menu \"Diikuti\"", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {action in
            print("Terdaftar")
        }))
        present(alert, animated: true)
    }
}
extension DetailProgramViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath ) {
        if indexPath.row == 0 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "NgoProfilePage", bundle:nil)
            if let ngoprofile = storyBoard.instantiateViewController(identifier: "PenyelenggaraViewController") as? PenyelenggaraViewController{
                for (indeks,data) in penyelenggaraList.enumerated(){
                    if data.nama.lowercased().contains(program.penyelenggara.lowercased()){
                        defaults.set(indeks,forKey: "Indeks NGO")
                    }
                }
                
                self.navigationController?.pushViewController(ngoprofile, animated: true)
            }
        }else{
            detail_collection.deselectItem(at: indexPath, animated: true)}
    }
}

extension DetailProgramViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetProgCollectionViewCell.indentifier, for: indexPath) as? DetProgCollectionViewCell{
            if indexPath.row == 0 {
                cell.judul.text = "Penyelenggara:"
                cell.isi.text = program.penyelenggara
                return cell
            }
            else if indexPath.row == 1 {
                cell.judul.text = "Deskripsi:"
                cell.isi.text = program.deskripsi
                return cell
            }
            else if indexPath.row == 2 {
                cell.judul.text = "Kategori Volunteer:"
                cell.isi.text = ""
                for (index, value) in program.kategori.enumerated(){
                    if index != (program.kategori.count - 1) {
                        cell.isi.text! += "-\(value) \n"
                    }else {
                        cell.isi.text! += "-\(value)"
                    }
                }
                return cell
            }
            else if indexPath.row == 3 {
                cell.judul.text = "Kualifikasi (Rekomendasi):"
                cell.isi.text = ""
                for (index, value) in program.kualifikasi.enumerated(){
                    if index != (program.kualifikasi.count - 1) {
                        cell.isi.text! += "-\(value) \n"
                    }else {
                        cell.isi.text! += "-\(value)"
                    }
                    
                }
                return cell
            }
            else if indexPath.row == 4 {
                cell.judul.text = "Tanggal Pelaksanaan:"
                cell.isi.text = program.tanggal
                return cell
            }
            
        }
        return UICollectionViewCell()
    }
    
    
}






