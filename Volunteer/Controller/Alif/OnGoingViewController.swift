//
//  OnGoingViewController.swift
//  Home Appstore
//
//  Created by Ahmad Nur Alifullah on 26/07/21.
//

import UIKit

    
class OnGoingViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var id : [Int]!
    var sections :[Section] = SectionManager().section
    var programs : [Program] = ProgramManager().program
    var program : [Program] = []
    var idadata : [Int]!
    var isregis : Bool!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var noticeView: UIView!
    @IBOutlet weak var regisButton: UIButton!
    var issregis : Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        regisButton.layer.cornerRadius = 15
        regisButton.layer.borderColor = UIColor.black.cgColor
        regisButton.clipsToBounds = true
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        idadata = defaults.object(forKey: "id") as? [Int]
        if idadata != nil{
            id = idadata
        }else {
            id = []
        }
        interfaceFunc()
    }
    override func viewDidAppear(_ animated: Bool) {
        issregis = defaults.object(forKey: "Registrasi") as? Bool
        if issregis != nil{
            isregis = issregis
        }else {
            isregis = false
        }
        idadata = defaults.object(forKey: "id") as? [Int]
        if idadata != nil{
            id = idadata
        }else {
            id = []
        }
        interfaceFunc()
    }
    func interfaceFunc (){
        if isregis {
            if id.count == 0 {
                collectionView.isHidden = true
                label.text = "Nothing to show"
                regisButton.isHidden = true
//                label.textAlignment = .center
//        //            label.backgroundColor = .red  // Set background color to see if label is centered
//                label.translatesAutoresizingMaskIntoConstraints = false
//                self.view.addSubview(label)
//
//                let widthConstraint = NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal,
//                                                         toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250)
//                let heightConstraint = NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal,
//                                                          toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
//                let xConstraint = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
//                let yConstraint = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
//                NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
            }else{
                noticeView.isHidden = true
                collectionView.isHidden = false
                program=[]
                for i in id {
                    for j in programs {
                        if i == j.id {
                            program.append(j)
                        }
                    }
                }
                collectionView.frame = view.bounds
                let layout = UICollectionViewFlowLayout()
                layout.itemSize = CGSize(width: 349 , height: 100)
                collectionView.collectionViewLayout = layout
                collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                collectionView.backgroundColor = .systemBackground
                collectionView.register(MediumTableCell.nib(), forCellWithReuseIdentifier: MediumTableCell.reuseIdentifier)
                collectionView.delegate = self
                collectionView.dataSource = self
            }
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = UIColor.systemGray6
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }else{
            collectionView.isHidden = true
            regisButton.isHidden = false
            label.text = "Mohon masuk untuk melihat halaman ini"
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = UIColor.systemGray6
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            
        }
        
        
    }
    @IBAction func masukButtonTapped(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoginRegister", bundle:nil)
        if let ngoprofile = storyBoard.instantiateViewController(identifier: "MasukViewController") as? MasukViewController{
            self.navigationController?.pushViewController(ngoprofile, animated: true)
        }
    }
    
}

extension OnGoingViewController: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return program.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediumTableCell.reuseIdentifier, for: indexPath) as? MediumTableCell else {
            fatalError("Unable to dequeue \(MediumTableCell.self)")
        }
        
        cell.configure(with: program[indexPath.row])
        return cell
        
    }
}
extension OnGoingViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath ) {
       
        collectionView.deselectItem(at: indexPath, animated: true)
        if let detailProgramViewController = self.storyboard?.instantiateViewController(identifier: DetailProgramViewController.indentifier) as? DetailProgramViewController{
            detailProgramViewController.program = program[indexPath.row]
            self.navigationController?.pushViewController(detailProgramViewController, animated: true)
        }
    }
}

            

