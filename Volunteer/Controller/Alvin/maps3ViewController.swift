//
//  maps3ViewController.swift
//  Volunteer
//
//  Created by alvin hariyono on 26/07/21.
//

import UIKit
import MapKit
import CoreLocation

class maps3ViewController: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {
    static let indentifier = "maps3ViewController"
   // var simpan_coordinate : String!
    @IBOutlet weak var textFieldForAddress: UITextField!
    @IBOutlet weak var getDirectionsButton: UIButton!
    @IBOutlet weak var map: MKMapView!
    
    var locationManger = CLLocationManager()
    let locationManager = CLLocationManager()
    var regionInMeters: Double = 10000
    
   var simpan_alvin2 = ""
    
        override func viewDidLoad() {
        super.viewDidLoad()
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        coordinate_simpan()
        map.delegate = self
        checkLocationServices()
        getAddress()
         //   print(simpan_coordinate)
          
           // print(simpan_coordinate.simpan_alvin , "jnck2")
        // Do any additional setup after loading the view.
    }
    
    func coordinate_simpan() {
//        let simpan_alvin2 : String?
//        simpan_alvin2.simpan_alvin1 = {
//
//        }
        
        
        //simpan_coordinate = program.coordinate_save
        //simpan_coordinate = "-7.284400, 112.692534"
       // textFieldForAddress.text = simpan_coordinate
        textFieldForAddress.text = simpan_alvin2
    }
 
    @IBAction func getDirectionsTapped(_ sender: Any) {
        getAddress()
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(textFieldForAddress.text!) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
                else {
                    print("No Location Found")
                    return
            }
            print(location)
            self.mapThis(destinationCord: location.coordinate)
            
            let annotations = MKPointAnnotation()
                annotations.title = "Pinned"
            print("1")
            annotations.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude as! CLLocationDegrees, longitude: location.coordinate.longitude as! CLLocationDegrees)
            self.map.addAnnotation(annotations)
            print("2")
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        map.setRegion(region, animated: true)
        
        print(locations)
    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D) {
        
        let souceCordinate = (locationManger.location?.coordinate)!
        
        let soucePlaceMark = MKPlacemark(coordinate: souceCordinate)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Something is wrong :(")
                }
                return
            }
            
          let route = response.routes[0]
          self.map.addOverlay(route.polyline)
          self.map.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            
        }
        
        
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        return render
    }
   
    
    
    
   
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            map.setRegion(region, animated: true)
        }
    }
    
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
            
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            map.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            print("check 2")
            break
        case .denied:
            // Show alert instructing them how to turn on permissions
            
            print("check 3")

            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
            print("check 4")

        case .restricted:
            // Show an alert letting them know what's up
            print("check 5")

            break
        case .authorizedAlways:
            print("check 6")
            map.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        }
    }
    
   


        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            checkLocationAuthorization()
            print("check 1")
        }



    
}





    
    
    
    
//}
