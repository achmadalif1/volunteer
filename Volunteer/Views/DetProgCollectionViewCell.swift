//
//  DetProgCollectionViewCell.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 15/07/21.
//

import UIKit

class DetProgCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var judul: UILabel!
    @IBOutlet weak var isi: UILabel!
    static let indentifier = "DetProgCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.layer.backgroundColor = UIColor.white.cgColor
        contentView.layer.borderWidth = 0.2
        contentView.layer.borderColor = UIColor.gray.cgColor
        contentView.layer.cornerRadius = 10
    }
    static func nib() -> UINib {
        return UINib(nibName: "DetProgCollectionViewCell", bundle: nil)
    }

}
