//
//  MediumTableCell.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 23/07/21.
//

import UIKit

class MediumTableCell: UICollectionViewCell, SelfConfiguringCell {
    static let reuseIdentifier: String = "MediumTableCell"
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var panah: UILabel!
    @IBOutlet weak var innerStackView: UIStackView!
    @IBOutlet weak var outerStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        name.font = UIFont.preferredFont(forTextStyle: .headline)
        name.textColor = .label

        subtitle.font = UIFont.preferredFont(forTextStyle: .subheadline)
        subtitle.textColor = .secondaryLabel

        imageView.layer.cornerRadius = 15
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1
        
        
        imageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        panah.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        innerStackView.axis = .vertical
        
        outerStackView.translatesAutoresizingMaskIntoConstraints = false
        outerStackView.alignment = .center
        outerStackView.spacing = 10
        
        NSLayoutConstraint.activate([
            outerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            outerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            outerStackView.topAnchor.constraint(equalTo: contentView.topAnchor)
        ])

    }
    static func nib() -> UINib {
        return UINib(nibName: "MediumTableCell", bundle: nil)
    }
    func configure(with program: Program) {
        name.text = program.nama
        subtitle.text = "\(program.lokasi) \(program.jarak) km"
        imageView.image = UIImage(named: program.photoAssetName)
    }

}
