//
//  FeaturedCell.swift
//  Home Appstore
//
//  Created by Ahmad Nur Alifullah on 24/07/21.
//

import UIKit

class FeaturedCell: UICollectionViewCell, SelfConfiguringFeatureCell {
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var innerStackview: UIStackView!
    
    static let reuseIdentifier: String = "FeaturedCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let separator = UIView(frame: .zero)
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = .quaternaryLabel

        tagline.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12, weight: .bold))
        tagline.textColor = .systemBlue

        name.font = UIFont.preferredFont(forTextStyle: .title2)
        name.textColor = .label

        subtitle.font = UIFont.preferredFont(forTextStyle: .title3)
        subtitle.textColor = .secondaryLabel

        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1

        let stackView = UIStackView(arrangedSubviews: [separator, innerStackview, imageView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        contentView.addSubview(stackView)

        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: 1),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        stackView.setCustomSpacing(10, after: separator)
        stackView.setCustomSpacing(10, after: subtitle)
    }
    static func nib() -> UINib {
        return UINib(nibName: "FeaturedCell", bundle: nil)
    }

    func configure(with app: Penyelenggara) {
        tagline.text = app.kota.uppercased()
        name.text = app.nama
        subtitle.text = ""
        for (index,focus) in app.fokus.enumerated(){
            if index != (app.fokus.count - 1) {
                subtitle.text?.append("\(focus), ")
            }else{
                subtitle.text?.append("\(focus)")
            }
            
        }
        
        imageView.image = UIImage(named: app.foto)
    }

}
