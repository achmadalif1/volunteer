//
//  SectionHeader.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 23/07/21.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    static let reuseIdentifier = "SectionHeader"
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var seeallButton: UIButton!
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var stackView: UIStackView!
    var indeks : Int!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let separator = UIView(frame: .zero)
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = .quaternaryLabel

        title.textColor = .label
        title.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 22, weight: .bold))
        subtitle.textColor = .secondaryLabel
        
//        let stackView1 = UIStackView(arrangedSubviews: [title, seeallButton])
//        stackView1.axis = .horizontal
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        
        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: 1),

            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
                                        ])
        
        stackView.setCustomSpacing(10, after: separator)

    }
    @IBAction func seeall(_ sender: Any) {
        print("halo")
    
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "SectionHeader", bundle: nil)
    }
}
