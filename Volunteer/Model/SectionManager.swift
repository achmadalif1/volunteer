//
//  SectionManager.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 23/07/21.
//

import Foundation


public class SectionManager {
    var section: [Section]
    init() {
        section = [
            Section(id: 1, type: "featureTable", title: "Minat", subtitle: "Temukan project sesuai minatmu", items: ProgramManager().program),
            Section(id: 2, type: "mediumTable", title: "Lokasi", subtitle: "Temukan project sesuai lokasi terdekatmu", items: finder(type: "lokasi")),
            Section(id: 3, type: "mediumTable", title: "Minat", subtitle: "Temukan project sesuai minatmu", items: finder(type: "minat"))
            
                    
//            Section(id: 3, type: "smallTable", title: "Sesuai Minatmu", subtitle: "Temukan project favoritmu", items: ProgramManager().program)
            
        ]
    }
}

func finder(type : String) ->[Program] {
    var program:[Program] = ProgramManager().program
    var filtered : [Program] = []
    var minatt : [String]!
    minatt = defaults.object(forKey: "Minat") as? [String] ?? [String]()
    if type == "minat"{
        if (minatt != nil) {
            var found : Bool = false
            for data in program {
                found = false
                for i in data.kategori{
                    for j in minatt{
                        if i.lowercased().contains(j.lowercased()){
                            found = true
                        }
                    }
                }
                if found == true{
                    filtered.append(data)
                }
                
            }
            return filtered
        }else {
            return filtered
        }
    }else if (type == "lokasi"){
        filtered = program.sorted(by: { $0.jarak < $1.jarak })
    }
    return filtered
    
    
}
