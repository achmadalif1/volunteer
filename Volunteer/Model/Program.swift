//
//  Program.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 14/07/21.
//

import Foundation
public struct Program : Hashable{
    var id : Int
    var nama: String
    var lokasi: String
    var jarak : Int
    var penyelenggara: String
    var deskripsi: String
    var photoAssetName: String
    var kategori : [String]
    var kualifikasi : [String]
    var tanggal : String
    var daftar : Bool
    var coordinate_save : String
}

