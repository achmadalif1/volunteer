//
//  Minat.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 19/07/21.
//

import Foundation

struct Minat: Identifiable {
    var id: Int
    var pilihan: [String]
}

var pilihanMinat =
[
    "Dunia Tanpa Kemiskinan",
    "Dunia Tanpa Kelaparan",
    "Kesehatan Fisik",
    "Kesehatan Mental",
    "Pendidikan Berkualitas",
    "Kesetaraan Gender",
    "Pemberdayaan Perempuan",
    "Akses Air Bersih dan Sanitasi",
    "Energi Bersih dan Terjangkau",
    "Pekerjaan yang Layak",
    "Pertumbuhan Ekonomi",
    "Kesejahteraan Merata",
    "Penanganan Perubahan Iklim",
    "Menjaga Ekosistem Laut",
    "Menjaga Ekosistem Hutan",
    "Menjaga Keberlangsungan Hidup Tumbuhan dan Hewan",
    "Menjaga Perdamaian Dunia",
    "Kerjasama Internasional",
]
