//
//  Aktivitas.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 23/07/21.
//

import Foundation

struct History: Identifiable {
    var id: Int
    var nama: String
    var tanggal: String
}

 var historyList = [
    History(id: 0, nama: "Sumber Daya Alam Laut Kita", tanggal: "25 Juli 2021 - 30 Juli 2021"),
    History(id: 1, nama: "Relawan Generasi Bebas Covid-19", tanggal: "15 Juli 2021"),
    History(id: 2, nama: "Campus Hero", tanggal: "10 Juli 2021 - 11 Juli 2021"),
    History(id: 3, nama: "Vaksinator Vaksin", tanggal: "1 Juli 2021 - 3 Juli 2021"),
]

