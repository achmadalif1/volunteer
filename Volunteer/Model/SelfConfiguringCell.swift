//
//  SelfConfiguringCell.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 23/07/21.
//

import Foundation
protocol SelfConfiguringCell {
    static var reuseIdentifier: String { get }
    func configure(with program: Program)
}
protocol SelfConfiguringFeatureCell {
    static var reuseIdentifier: String { get }
    func configure(with program: Penyelenggara)
}
