//
//  File.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 23/07/21.
//

import Foundation
public struct Section : Hashable {
    let id: Int
    let type: String
    let title: String
    let subtitle: String
    let items: [Program]
}
