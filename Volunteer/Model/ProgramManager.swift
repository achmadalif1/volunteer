//
//  ProgramManager.swift
//  Volunteer
//
//  Created by Ahmad Nur Alifullah on 14/07/21.
//

import Foundation
public class ProgramManager {
    var program: [Program]
    init() {
        program = [
            Program(
                id: 1,
                nama: "Relawan Diskusi \"Forum Kesaksian Korban\"",
                lokasi: "Surabaya, Jawa Timur",
                jarak: 100,
                penyelenggara: "PeaceWomen Across the Globe (PWAG) Indonesia",
                deskripsi: "Sebagai rangkaian peringatan Hari Perempuan Sedunia (08/03), PWAG bersama kelompok perempuan lainnya berinisiatif membuat kampanye bersama dengan tema “Merajut Solidaritas Bagi Korban Kekerasan Seksual”. Di tanggal 14 April, PWAG & Jurusan Filsafat FIB UI mengadakan Forum Kesaksian Korban untuk meningkatkan kesadaran masyarakat akan situasi dan kondisi korban kekerasan.",
                photoAssetName: "photo1",
                kategori : ["Kesetaraan Gender"],
                kualifikasi : ["S1 di semua jurusan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(
                id: 2,
                nama: "#BackToLerak Campaigner",
                lokasi: "Jombang, Jawa Timur",
                jarak: 120,
                penyelenggara: "WateryNation (Thirst Project Indonesia)",
                deskripsi: "Retro gak cuma trend tapi retro bisa jadi trend yang ramah lingkungan! Goes retro with @thirstproject.id bakal ajak kamu menjadi campaigner Lerak di lingkungan rumahmu!",
                photoAssetName: "photo2",
                kategori : ["Akses Air Bersih dan Sanitasi"],
                kualifikasi : ["S1 di bidang yang berhubungan dengan kesehatan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(
                id: 3,
                nama: "Digital Give: Contribute your Digital Skill",
                lokasi: "Surabaya, Jawa Timur",
                jarak: 157,
                penyelenggara: "WateryNation (Thirst Project Indonesia)",
                deskripsi: "Punya skill IT + jiwa sosial? boleh banget kontribusiin skill kamu dalam digital, kali ini kita membuka peluang bagi kamu para pembuat website untuk memberikan jasa pembuatan website gratis* untuk mendukung NGO Go Digital! 1 bulan = membantu 1 NGO \n *Gratis jasa. Untuk domain, hosting dan lainnya dibayar oleh NGO terkait atau silakan jika ingin berkontribusi lebih",
                photoAssetName: "photo3",
                kategori : ["Akses Air Bersih dan Sanitasi"],
                kualifikasi : ["S1 di bidang yang berhubungan dengan kesehatan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(
                id: 4,
                nama: "Menghadap Laut: Pantai Poka, Ambon",
                lokasi: "Surabaya, Jawa Timur",
                jarak: 157,
                penyelenggara: "Pandu Laut Nusantara",
                deskripsi: "70% wilayah Indonesia merupakan lautan. Bentang laut Indonesia dari Sabang sampai Merauke memiliki keanekaragaman hayati yang begitu kaya. Namun, kondisi laut Indonesia kini terancam sampah, dalam catatan KIARA, ada sedikitnya 1,29 juta ton sampah mencemari laut kita setiap tahunnya. Selama ini kepedulian kita akan laut masih sangat minim. Padahal laut Indonesia merupakan warisan penting bagi generasi selanjutnya. Akankah kita terus berdiam diri melihat laut kita hancur? \n Menghadap Laut akan diadakan di 74 lebih lokasi pantai dan laut yang sudah ditentukan oleh panitia. Relawan diharapkan memilih lokasi yang dekat dengan domisili relawan. Bersama dengan Menteri Susi Pujiastuti, Kaka Slank, Ridho Hafidz, Ony Seroja, Bustar Maitar, Brahmantya S. Poerwadi, Aprika Rani, Prita Laura, Tiza Mafira, Andien Aisyah, Marcell Siahaan, Monita Tahalea, serta ribuan pecinta laut lainnya, para relawan akan diajak untuk membersihkan pantai dan laut. Kemudian dilanjutkan dengan makan ikan bersama-sama.",
                photoAssetName: "photo4",
                kategori : ["Menjaga Ekosistem Laut"],
                kualifikasi : ["S1 di bidang yang berhubungan dengan kesehatan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(
                id: 5,
                nama: "Relawan Farm Animal Welfare Indonesia",
                lokasi: "Surabaya, Jawa Timur",
                jarak: 157,
                penyelenggara: "Farm Animal Welfare Indonesia Watch",
                deskripsi: "Ini adalah deskripsi aktivitas volunteer. \n Tentunya, deskripsi ini akan memakan lebih dari 1 kalimat.",
                photoAssetName: "photo5",
                kategori : ["Menjaga Keberlangsungan Hidup Tumbuhan dan Hewan"],
                kualifikasi : ["S1 di bidang yang berhubungan dengan kesehatan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(
                id: 6,
                nama: "Jadi Relawan Perawat Alam Indonesia (RAWALI)",
                lokasi: "Surabaya, Jawa Timur",
                jarak: 157,
                penyelenggara: "Global Peace Youth Indonesia Chapter Surabaya",
                deskripsi: "Semua manusia di belahan dunia manapun menepati tempat yang sama yaitu Bumi yang merupakan alam yang harus dilestarikan dan dipelihara oleh pengelolanya yaitu manusia. Kondisi bumi pada saat ini dalam kritis tingkat akut dikarenakan : kurangnya perhatian manusia yang peduli lingkungan dan merawat alam tempat tinggal mereka. Sehingga terjadi penumpukan sampah-sampah plastik berbahan kimia, dan sampah-sampah limbah rumah tangga hingga memicu faktor kurangnya ketersediaan air bersih serta polusi udara dan pemanasan global akibat penebangan liar hutan sembarangan, sehingga semakin berpengaruh terhadap kesehatan lingkungan dan alam khususnya di Indonesia. \n Oleh sebab itu Komunitas Laskar Damai Indonesia, sebagai komunitas penggerak pengembang generasi muda, mengajak generasi muda untuk dapat menjadi Relawan Perawat Alam Indonesia untuk dapat memulihkan kesehatan bumi dan alam Indonesia agar dapat dinikmati oleh genrasi berikutnya dan masyarakat yang juga akan merasakan dampaknya. \n Adapun fokus kegiatan RAWALI (Relawan Perawat Alam Indonesia) ada dalam tiga titik fokus : \n 1. Pelestarian lingkungan dan penanaman pohon di berbagai daerah di seluruh Indonesia \n 2. Pembersihan perairan demi terciptanya air bersih di seluruh daerah di Indonesia, serta \n 3. Bijak mengelola, dan membuang sampah di seluruh daerah di Indonesia \n Kegiatan ini dilaksanakan secara daring dengan edukasi generasi muda mengenai cara-cara dan langkah-langkah yang harus dilakukan di lapangan (daerah masing-masing) untuk menjadi RAWALI (Relawan Perawat Alam Indonesia).",
                photoAssetName: "photo6",
                kategori : ["Menjaga Keberlangsungan Hidup Tumbuhan dan Hewan"],
                kualifikasi : ["S1 di bidang yang berhubungan dengan kesehatan", "Mampu bekerja sama dalam tim"],
                tanggal : "31 Juli 2021 - 3 Agustus 2021",
                daftar : false,
                coordinate_save : "-7.284400, 112.692534"),
            Program(id: 7, nama: "Vaksinator Vaksin Covid-19", lokasi: "Surabaya, Jawa Timur", jarak: 100, penyelenggara: "Yayasan Tunas Bakti Nusantara", deskripsi: "Halo terima kasih sudah menghubungi Yayasan Tunas Bakti Nusantara 😊. Kriteria tenaga kesehatan yang dapat turut serta dalam program ini adalah berprofesi sebagai Dokter Umum (Internship/Post Internship), Perawat serta Mahasiswa Kedokteran ya kak. Mari bersama-sama sukseskan program percepatan vaksin Pemerintah Indonesia. Vaksin Merata, Sehatkan Indonesia. Sukseskan Vaksinasi, Indonesia Pulih Kembali!", photoAssetName: "photo7", kategori: ["Kesehatan Fisik"], kualifikasi: ["Mampu bekerja sama dalam tim"], tanggal: "31 Juli 2021 - 3 Agustus 2021", daftar : false, coordinate_save : "-7.284400, 112.692534"),
            
            Program(id: 8, nama: "Happiness Ambassador Batch 4", lokasi: "Surabaya, Jawa Timur", jarak: 100, penyelenggara: "#ISmile4You", deskripsi: "#ISmile4You melalui Happiness Ambassador perlu bantuan kamu untuk menyebarkan awareness tentang bully. Jika kamu peduli dengan isu bully yang masih marak terjadi, yuk bergabung dengan Happiness Ambassador!", photoAssetName: "photo8", kategori: ["Kesehatan Mental"], kualifikasi: ["Mampu bekerja sama dalam tim"], tanggal: "31 Juli 2021 - 3 Agustus 2021", daftar : false,
                    coordinate_save : "-7.284400, 112.692534"),
            
            Program(id: 9, nama: "GERAKAN SUKABUMI MENGJAR (GSM) BATCH VII", lokasi: "Surabaya, Jawa Timur", jarak: 100, penyelenggara: "Nusantara Terdidik", deskripsi: "Hallo sobat!\nSalam Pendidikan!\n Gerakan Sukabumi Mengajar (GSM) mengajakmu terlibat dalam program pendidikan untuk membersamai kawan-kawan kita di pelosok Sukabumi. \n Yuk, kini saatnya lebih terbuka dan membuka mata untuk melihat pendidikan di pelosok negeri dan turut terlibat untuk saling berbagi energi!", photoAssetName: "photo9", kategori: ["Pendidikan Berkualitas"], kualifikasi: ["Mampu bekerja sama dalam tim"], tanggal: "31 Juli 2021 - 3 Agustus 2021", daftar : false,
                    coordinate_save : "-7.284400, 112.692534"),
            
            Program(id: 10, nama: "MUARA: Mengasuh Air Kita", lokasi: "Surabaya, Jawa Timur", jarak: 100, penyelenggara: "MUARA", deskripsi: "Kita menuju krisis air dan kita butuh kamu! \n Water is life, dan hari ini kita menjumpai risiko krisis air yang semakin besar akibat aktivitas manusia & akibat krisis iklim. Berangkat dari sana, kami menginisiasi kegiatan kampanye selama 4 bulan (Juli-Oktober 2021) mengenai risiko krisis air khususnya untuk pelajar SD dan SMP. \n Kami percaya, memulai dari anak-anak bisa membawa isu ini lebih jauh, terutama dengan mengubah kebiasaan. Kita juga jadi belajar untuk menyampaikan pesan-pesan penting ini dengan lebih sederhana dan bisa dimengerti lebih banyak orang. \n We need around 10 volunteers to join our team!\n Limited slots, so apply as soon as possible!", photoAssetName: "photo10", kategori: ["Akses Air Bersih dan Sanitasi"], kualifikasi: ["Mampu bekerja sama dalam tim"], tanggal: "31 Juli 2021 - 3 Agustus 2021", daftar : false, coordinate_save : "-7.284400, 112.692534")
            ]
    }
}
