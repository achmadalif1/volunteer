//
//  Volunteer.swift
//  Volunteer
//
//  Created by Winner Tjandrawan on 16/07/21.
//

import Foundation


let defaults = UserDefaults.standard
let nama = defaults.string(forKey: "Nama")
let email = defaults.string(forKey: "Email")
let password = defaults.string(forKey: "Password")
let pekerjaan = defaults.string(forKey: "Pekerjaan")
var jumlahMinat = defaults.integer(forKey: "JumlahMinat")
var minat = defaults.object(forKey: "Minat") as? [String] ?? []
let foto = defaults.data(forKey: "Foto")

let isRegis = defaults.object(forKey: "Registrasi") as? Bool
let idData = defaults.object(forKey: "id") as? [Int]
let indeks = defaults.object(forKey: "indeks") as? Int
